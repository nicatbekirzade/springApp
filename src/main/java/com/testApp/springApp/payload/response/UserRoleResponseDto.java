package com.testApp.springApp.payload.response;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

@Getter
@Setter
public class UserRoleResponseDto {

    private Long id;

    private String permissions;

    private RoleResponseDto role;

    private UserResponseDto user;

    private LocalDateTime createdAt;

    private LocalDateTime updatedAt;

    private LocalDateTime deletedAt;

    private Boolean status;
}