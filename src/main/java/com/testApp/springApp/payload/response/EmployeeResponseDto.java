package com.testApp.springApp.payload.response;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class EmployeeResponseDto {

    private Long id;

    private String name;

    private String surname;

    private String email;

    private String image;

    private String address;

    private String contact;

    private String birthdate;

    private EducationResponseDto education;

    private LocalDateTime createdAt;

    private LocalDateTime updatedAt;

    private LocalDateTime deletedAt;

    private Boolean status;

}