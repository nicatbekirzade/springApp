package com.testApp.springApp.payload.request.roleDto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

@Getter
@Setter
@Validated
public class RoleRequestDto {

    @NotBlank(message = "Name should not be blank.")
    private String name;

    @NotBlank(message = "Default permissions should not be blank.")
    private String defaultPermissions;

    private Boolean status = true;

}