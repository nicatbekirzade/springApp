package com.testApp.springApp.payload.request.educationTrDto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import java.util.Map;

@Getter
@Setter
public class EducationTrRequestDto {


    @NotBlank(message = "Name should not be blank.")
    private Map<Long, String> name;

    @NotBlank(message = "Language should not be blank.")
    private Map<Long, Long> languageId;

    private Boolean status = true;


}
