package com.testApp.springApp.payload.request.userDto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;


@Getter
@Setter
@Validated
public class UserRequestDto {

    @NotBlank(message = "Employee should not be blank.")
    private Long employeeId;

    @NotBlank
    private String name;

    private Boolean status = true;

}