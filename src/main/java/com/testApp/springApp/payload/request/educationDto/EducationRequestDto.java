package com.testApp.springApp.payload.request.educationDto;


import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;


@Getter
@Setter
public class EducationRequestDto {

    private Boolean status = true;

}