package com.testApp.springApp.payload.request.languageDto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

@Getter
@Setter
@Validated
public class LanguageRequestDto {

    @NotBlank(message = "Name should not be blank.")
    private String name;

    @NotBlank(message = "Code should not be blank.")
    private String code;

    private Boolean status = true;

}