package com.testApp.springApp.payload.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
public class UserRoleRequestDto {

    @NotBlank(message = "Permissions required!")
    private String permissions;

    @NotBlank(message = "Roles required!")
    private Long roleId;

    @NotBlank(message = "Select any User!")
    private Long userId;

    private Boolean status = true;
}