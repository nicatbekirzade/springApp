package com.testApp.springApp.controller;

import com.testApp.springApp.payload.request.languageDto.LanguageRequestDto;
import com.testApp.springApp.payload.response.LanguageResponseDto;
import com.testApp.springApp.services.LanguageService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/auth/language")
public class LanguageController {

    private final LanguageService languageService;

    @GetMapping("find/{id}")
    public LanguageResponseDto findById(@PathVariable("id") Long id){
        return languageService.findById(id);
    }


    @PostMapping("create")
    public LanguageResponseDto createLanguage(@RequestBody LanguageRequestDto languageRequestDto){
        return languageService.createLanguage(languageRequestDto);
    }

    @PutMapping("update/{id}")
    public LanguageResponseDto updateLanguage(@PathVariable("id") Long id, @RequestBody LanguageRequestDto languageRequestDto){
        return languageService.updateLanguage(id, languageRequestDto);
    }

    @DeleteMapping("delete/{id}")
    public void deleteById(@PathVariable("id") Long id){
        languageService.deleteById(id);
    }
}
