package com.testApp.springApp.controller;

import com.testApp.springApp.payload.request.educationTrDto.EducationTrRequestDto;
import com.testApp.springApp.payload.response.EducationTrResponseDto;
import com.testApp.springApp.services.EducationTrService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/auth/educationTr")
@RequiredArgsConstructor
public class EducationTrController {

    private final EducationTrService educationTrService;

    @PostMapping("create")
    public List<EducationTrResponseDto> createEducationTr(@RequestBody EducationTrRequestDto educationTrRequestDto){
        return educationTrService.createEducationTr(educationTrRequestDto);
    }

    @GetMapping("find/{id}")
    public List<EducationTrResponseDto> findById(@PathVariable("id") Long id){
        return educationTrService.findById(id);
    }


    @PutMapping("update/{id}")
    public List<EducationTrResponseDto> updateEducationTr(@PathVariable("id") Long id ,
                                                          @RequestBody EducationTrRequestDto educationTrRequestDto){
        return educationTrService.updateEducationTr(id, educationTrRequestDto);
    }



}
