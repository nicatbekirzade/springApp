package com.testApp.springApp.controller;

import com.testApp.springApp.payload.request.educationDto.EducationRequestDto;
import com.testApp.springApp.payload.response.EducationResponseDto;
import com.testApp.springApp.services.EducationService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/auth/education")
public class EducationController {

    private final EducationService educationService;

    @PostMapping("create")
    public EducationResponseDto createEducation(@RequestBody EducationRequestDto educationRequestDto){
        return educationService.createEducation(educationRequestDto);
    }

    @GetMapping("find/{id}")
    public EducationResponseDto findById(@PathVariable("id") Long id){
        return educationService.findById(id);
    }

    @DeleteMapping("delete/{id}")
    public void deleteEducation(@PathVariable("id") Long id){
        educationService.deleteEducation(id);
    }

}
