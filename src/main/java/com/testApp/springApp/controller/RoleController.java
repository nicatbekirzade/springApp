package com.testApp.springApp.controller;

import com.testApp.springApp.payload.request.roleDto.RoleRequestDto;
import com.testApp.springApp.payload.response.RoleResponseDto;
import com.testApp.springApp.services.RoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/auth/role")
@RequiredArgsConstructor
public class RoleController {

    private final RoleService roleService;

    @PostMapping("create")
    public RoleResponseDto createRole(@RequestBody RoleRequestDto roleRequestDto) {
        return roleService.createRole(roleRequestDto);
    }

    @GetMapping("find/{id}")
    public RoleResponseDto findById(@PathVariable("id") Long id) {
        return roleService.findById(id);
    }

    @PutMapping("update/{id}")
    public RoleResponseDto updateRole(@PathVariable("id") Long id, @RequestBody RoleRequestDto roleRequestDto) {
        return roleService.updateRole(id, roleRequestDto);
    }

    @DeleteMapping("delete/{id}")
    public void deleteRole(@PathVariable("id") Long id) {
        roleService.deleteById(id);
    }
}
