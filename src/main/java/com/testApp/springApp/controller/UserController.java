package com.testApp.springApp.controller;

import com.testApp.springApp.payload.request.userDto.UserRequestDto;
import com.testApp.springApp.payload.response.UserResponseDto;
import com.testApp.springApp.services.UserService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/auth/user")
//@RequiredArgsConstructor
public class UserController {


    private UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }


    @PostMapping("create")
    public UserResponseDto createUser(@RequestBody UserRequestDto userRequestDto) {
        return userService.createUser(userRequestDto);
    }


    @PutMapping("update/{id}")
    public UserResponseDto updateEmployee(@PathVariable("id") Long id,
                                          @RequestBody UserRequestDto userRequestDto) {
        return userService.updateUser(id, userRequestDto);
    }

    @GetMapping("find/{id}")
    public UserResponseDto findById(@PathVariable("id") Long id) {
        return userService.findById(id);
    }

    @DeleteMapping("delete/{id}")
    public void deleteUser(@PathVariable("id") Long id) {
        userService.deleteUser(id);
    }


}
