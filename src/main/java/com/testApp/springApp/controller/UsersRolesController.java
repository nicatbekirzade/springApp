package com.testApp.springApp.controller;

import com.testApp.springApp.payload.request.UserRoleRequestDto;
import com.testApp.springApp.payload.response.UserRoleResponseDto;
import com.testApp.springApp.services.UserRoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/auth/usersRoles")
@RequiredArgsConstructor
public class UsersRolesController {

    private final UserRoleService userRoleService;

    @PostMapping("create")
    public UserRoleResponseDto createUserRole(@RequestBody UserRoleRequestDto userRoleRequestDto) {
        return userRoleService.createUserRole(userRoleRequestDto);
    }

    @GetMapping("find/{id}")
    public UserRoleResponseDto findById(@PathVariable("id") Long id) {
        return userRoleService.findById(id);
    }

    @PutMapping("update/{id}")
    public UserRoleResponseDto updateUserRole(@PathVariable("id") Long id, @RequestBody UserRoleRequestDto userRoleRequestDto) {
        return userRoleService.updateUserRole(id, userRoleRequestDto);
    }


//    public List<UsersRoles> getAll(Long id){
//        return usersRolesService.getAll(id);
//    }

}
