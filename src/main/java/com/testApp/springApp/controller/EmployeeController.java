package com.testApp.springApp.controller;


import com.testApp.springApp.payload.request.employeeDto.EmployeeRequestDto;
import com.testApp.springApp.payload.response.EmployeeResponseDto;
import com.testApp.springApp.services.EmployeeService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/auth/emp")
@RequiredArgsConstructor
public class EmployeeController {

    private final EmployeeService employeeService;

    @PostMapping("create")
    public EmployeeResponseDto createEmployee(@RequestBody EmployeeRequestDto employeeRequestDto) {
        return employeeService.createEmployee(employeeRequestDto);
    }

    @GetMapping("/find/{id}")
    public EmployeeResponseDto findById(@PathVariable("id") Long id) {
        return employeeService.findById(id);
    }

    @PutMapping("/update/{id}")
    public EmployeeResponseDto updateEmployee(@PathVariable("id") Long id,
                                              @RequestBody EmployeeRequestDto employeeRequestDto) {
        return employeeService.updateEmployee(id, employeeRequestDto);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteEmployee(@PathVariable("id") Long id) {
        employeeService.deleteEmployee(id);
    }
}
