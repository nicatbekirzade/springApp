package com.testApp.springApp.services;


import com.testApp.springApp.payload.request.userDto.UserRequestDto;
import com.testApp.springApp.payload.response.EmployeeResponseDto;
import com.testApp.springApp.payload.response.UserResponseDto;
import com.testApp.springApp.exceptions.UserNotFoundEx;
import com.testApp.springApp.model.Employee;
import com.testApp.springApp.model.User;

import com.testApp.springApp.repository.UserRepo;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.Random;

@Log4j2
@Service
@RequiredArgsConstructor
public class UserService {

    private final EmployeeService employeeService;
    private final EmailSenderService emailSenderService;
    private final UserRepo userRepo;
    private final ModelMapper modelMapper;

    public UserResponseDto findById(Long id) {
        UserResponseDto userResponseDto = new UserResponseDto();
        Optional<User> byId = userRepo.findById(id);
        if (byId.isPresent()) {
            BeanUtils.copyProperties(byId.get(), userResponseDto);
            return userResponseDto;
        } else throw new UserNotFoundEx();
    }

    public UserResponseDto createUser(UserRequestDto userRequestDto){
        //find and convert EmployeeDto to Employee
        EmployeeResponseDto employeeResponseDto = employeeService.findById(userRequestDto.getEmployeeId());
        Employee employee = modelMapper.map(employeeResponseDto, Employee.class);

        //convert UserDto to User
        User user = modelMapper.map(userRequestDto, User.class);
        user.setEmployee(employee);

        //generate and send password
        String password = generatePassword();
        System.out.println(employee.getEmail());
        sendEmail(employee.getEmail(),password);

        //encode and set Password
        user.setPassword(password);
        userRepo.save(user);

        return modelMapper.map(user, UserResponseDto.class);
    }

    public UserResponseDto updateUser(Long id, UserRequestDto userRequestDto){
        Optional<User> byId= userRepo.findById(id);
        if(byId.isPresent()){
            User user = modelMapper.map(userRequestDto, User.class);
            user.setId(id); // TODO check this line if id not set
            userRepo.save(user);
            return modelMapper.map(user, UserResponseDto.class);
        }else throw new UserNotFoundEx();

    }

    public void deleteUser(Long id){
        Optional<User> byId = userRepo.findById(id);
        if(byId.isPresent() && byId.get().getStatus()){
            User education = byId.get();
            education.setStatus(false);
            education.setDeletedAt(LocalDateTime.now());
            userRepo.save(education);
        }
    }

    private String generatePassword() {
        Random random = new Random();
        int password = random.nextInt(899)+1000;
        boolean exists = userRepo.existsByPassword(String.valueOf(password));
        if(!exists) return String.valueOf(password);
        else return generatePassword();
    }

    private void sendEmail(String email,String password) {
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(email);
        mailMessage.setSubject("Password.");
        mailMessage.setFrom("businesshub808@gmail.com");
        mailMessage.setText("Your password: " + password);
        emailSenderService.sendEmail(mailMessage);
    }
}
