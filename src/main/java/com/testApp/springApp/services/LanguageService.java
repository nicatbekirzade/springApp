package com.testApp.springApp.services;

import com.testApp.springApp.payload.request.languageDto.LanguageRequestDto;
import com.testApp.springApp.payload.response.LanguageResponseDto;
import com.testApp.springApp.exceptions.LanguageAlreadyExistsEx;
import com.testApp.springApp.exceptions.LanguageNotFoundEx;
import com.testApp.springApp.model.Language;
import com.testApp.springApp.repository.LanguageRepo;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class LanguageService {

    private final LanguageRepo languageRepo;
    private final ModelMapper modelMapper;

    public LanguageResponseDto createLanguage(LanguageRequestDto languageRequestDto) {
        boolean existsByName = languageRepo.existsByName(languageRequestDto.getName());
        if (!existsByName) {
            Language language = modelMapper.map(languageRequestDto, Language.class);
            languageRepo.save(language);
            return modelMapper.map(language, LanguageResponseDto.class);
        } else throw new LanguageAlreadyExistsEx();
    }

    public LanguageResponseDto findById(Long id) {
        Optional<Language> byId = languageRepo.findById(id);
        if (byId.isPresent() && byId.get().getStatus()) {
            return modelMapper.map(byId.get(), LanguageResponseDto.class);
        } else throw new LanguageNotFoundEx();
    }

    public LanguageResponseDto updateLanguage(Long id, LanguageRequestDto languageRequestDto) {
        Optional<Language> byId = languageRepo.findById(id);
        if (byId.isPresent() && byId.get().getStatus()) {
            Language language = byId.get();
            BeanUtils.copyProperties(languageRequestDto, language);
            languageRepo.save(language);
            return modelMapper.map(language, LanguageResponseDto.class);
        } else throw new LanguageNotFoundEx();
    }

    public void deleteById(Long id) {
        languageRepo.deleteById(id);
//        Optional<Language> byId = languageRepo.findById(id);
//        if (byId.isPresent() && byId.get().getStatus()) {
//            Language language = byId.get();
//            language.setStatus(false);
//            language.setDeletedAt(LocalDateTime.now());
//            languageRepo.save(language);
//        }
    }
}
