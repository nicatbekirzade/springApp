package com.testApp.springApp.services;

import com.testApp.springApp.payload.request.roleDto.RoleRequestDto;
import com.testApp.springApp.payload.response.RoleResponseDto;
import com.testApp.springApp.exceptions.RoleAlreadyExistEx;
import com.testApp.springApp.exceptions.RoleNoteFoundEx;
import com.testApp.springApp.model.Role;
import com.testApp.springApp.repository.RoleRepo;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class RoleService {

    private final RoleRepo roleRepo;
    private final ModelMapper modelMapper;

    public RoleResponseDto createRole(RoleRequestDto roleRequestDto) {
        boolean existsByName = roleRepo.existsByName(roleRequestDto.getName());
        if (!existsByName) {
            Role role = modelMapper.map(roleRequestDto, Role.class);
            roleRepo.save(role);
            return modelMapper.map(role, RoleResponseDto.class);
        } else throw new RoleAlreadyExistEx();
    }

    public RoleResponseDto findById(Long id) {
        Optional<Role> byId = roleRepo.findById(id);
        if (byId.isPresent()) {
            return modelMapper.map(byId.get(), RoleResponseDto.class);
        } else throw new RoleNoteFoundEx();

    }

    public RoleResponseDto updateRole(Long id, RoleRequestDto roleRequestDto) {
        Optional<Role> existingRole = roleRepo.findById(id);
        if (existingRole.isPresent()) {
            Role role = existingRole.get();
            BeanUtils.copyProperties(roleRequestDto, role);
            roleRepo.save(role);
           return modelMapper.map(role, RoleResponseDto.class);
        }
        throw new RoleNoteFoundEx();
    }

    public void deleteById(Long id) {
        roleRepo.deleteById(id);
//        Optional<Role> byId = roleRepo.findById(id);
//        if(byId.isPresent() && byId.get().getStatus()){
//            Role education = byId.get();
//            education.setStatus(false);
//            education.setDeletedAt(LocalDateTime.now());
//            roleRepo.save(education);
//        }
    }
}
