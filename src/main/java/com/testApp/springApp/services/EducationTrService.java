package com.testApp.springApp.services;

import com.testApp.springApp.payload.request.educationDto.EducationRequestDto;
import com.testApp.springApp.payload.request.educationTrDto.EducationTrRequestDto;
import com.testApp.springApp.payload.response.EducationResponseDto;
import com.testApp.springApp.payload.response.EducationTrResponseDto;
import com.testApp.springApp.exceptions.EducationNotFoundEx;
import com.testApp.springApp.model.Education;
import com.testApp.springApp.model.EducationTr;
import com.testApp.springApp.model.Language;
import com.testApp.springApp.repository.EducationTrRepo;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class EducationTrService {

    private final EducationTrRepo educationTrRepo;
    private final EducationService educationService;
    private final LanguageService languageService;
    private final ModelMapper modelMapper;

    public List<EducationTrResponseDto> createEducationTr(EducationTrRequestDto educationTrRequestDto) {
        Education education = new Education();
        EducationResponseDto existingEducationResponseDto = educationService.createEducation(new EducationRequestDto());
        BeanUtils.copyProperties(existingEducationResponseDto, education);

        List<EducationTrResponseDto> educationTrResponseDtoList = new ArrayList<>();

        for (Long key : educationTrRequestDto.getLanguageId()
                .keySet()) {
            EducationTr educationTr = new EducationTr();
            String educationName = educationTrRequestDto.getName().get(key);
            educationTr.setName(educationName);
            educationTr.setLanguage(modelMapper.map(languageService.findById(educationTrRequestDto.getLanguageId().get(key)), Language.class));
            educationTr.setEducation(education);
            educationTrRepo.save(educationTr);
            System.out.println(educationTr);
            educationTrResponseDtoList.add(modelMapper.map(educationTr, EducationTrResponseDto.class));
        }
        return educationTrResponseDtoList;
    }

    public List<EducationTrResponseDto> updateEducationTr(Long educationId, EducationTrRequestDto educationTrRequestDto) {
        List<EducationTrResponseDto> educationTrResponseDtoList = new ArrayList<>();

        for (Long key : educationTrRequestDto.getLanguageId()
                .keySet()) {
            Optional<EducationTr> byEducationIdAndLanguageId
                    = educationTrRepo.findByEducationIdAndLanguageId(educationId, educationTrRequestDto.getLanguageId().get(key));
            if (byEducationIdAndLanguageId.isPresent()) {
                EducationTr educationTr = byEducationIdAndLanguageId.get();
                educationTr.setName(educationTrRequestDto.getName().get(key));
                educationTrRepo.save(educationTr);
                educationTrResponseDtoList.add(modelMapper.map(educationTr, EducationTrResponseDto.class));
            } else throw new EducationNotFoundEx();
        }
        return educationTrResponseDtoList;
    }

    public List<EducationTrResponseDto> findById(Long id) {
        return educationTrRepo.findAllByEducationId(id)
                .stream()
                .map(educationTr -> modelMapper.map(educationTr, EducationTrResponseDto.class)).collect(Collectors.toList());
    }
}
