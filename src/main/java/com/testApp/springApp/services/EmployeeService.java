package com.testApp.springApp.services;


import com.testApp.springApp.payload.request.employeeDto.EmployeeRequestDto;
import com.testApp.springApp.exceptions.EmployeeAlreadyExistEx;
import com.testApp.springApp.exceptions.EmployeeNotFoundEx;
import com.testApp.springApp.model.Education;
import com.testApp.springApp.model.Employee;
import com.testApp.springApp.payload.response.EmployeeResponseDto;
import com.testApp.springApp.repository.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Log4j2
@Service
@RequiredArgsConstructor
public class EmployeeService {


    private final EmployeeRepo employeeRepo;
    private final EducationService educationService;
    private final ModelMapper modelMapper;

    public EmployeeResponseDto findById(Long id) {
        Optional<Employee> byId = employeeRepo.findById(id);
        if (byId.isPresent()) {
            return modelMapper.map(byId.get(), EmployeeResponseDto.class);
        } else throw new EmployeeNotFoundEx();
    }

    public EmployeeResponseDto createEmployee(EmployeeRequestDto employeeRequestDto) {
        boolean existByEmail = employeeRepo.existsByEmail(employeeRequestDto.getEmail());
        if (!existByEmail) {
            Employee employee = modelMapper.map(employeeRequestDto, Employee.class);
            Education education = modelMapper.map(educationService.findById(employeeRequestDto.getEducationId()), Education.class);
            employee.setEducation(education);
            employeeRepo.save(employee);
            return modelMapper.map(employee, EmployeeResponseDto.class);
        } else throw new EmployeeAlreadyExistEx();
    }

    public EmployeeResponseDto updateEmployee(Long id, EmployeeRequestDto employeeRequestDto) {
        Optional<Employee> byId = employeeRepo.findById(id);
        if (byId.isPresent()) {
            Employee employee = byId.get();
            BeanUtils.copyProperties(employeeRequestDto, employee);
            employeeRepo.save(employee);
            return modelMapper.map(employee, EmployeeResponseDto.class);
        } else throw new EmployeeNotFoundEx();
    }

    public void deleteEmployee(Long id) {
        Optional<Employee> byId = employeeRepo.findById(id);
        if (byId.isPresent() && byId.get().getStatus()) {
            Employee employee = byId.get();
            employee.setStatus(false);
            employee.setDeletedAt(LocalDateTime.now());
            employeeRepo.save(employee);
        }
    }


}