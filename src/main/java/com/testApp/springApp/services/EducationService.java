package com.testApp.springApp.services;

import com.testApp.springApp.payload.request.educationDto.EducationRequestDto;
import com.testApp.springApp.payload.response.EducationResponseDto;
import com.testApp.springApp.exceptions.EducationNotFoundEx;
import com.testApp.springApp.model.Education;
import com.testApp.springApp.repository.EducationRepo;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class EducationService {

    private final EducationRepo educationRepo;

    public EducationService(EducationRepo educationRepo) {
        this.educationRepo = educationRepo;
    }

    public EducationResponseDto findById(Long id) {
        EducationResponseDto educationResponseDto = new EducationResponseDto();
        Optional<Education> byId = educationRepo.findById(id);
        if (byId.isPresent()) {
            BeanUtils.copyProperties(byId.get(), educationResponseDto);
            return educationResponseDto;
        } else throw new EducationNotFoundEx();
    }

    public boolean existById(Long id) {
        return educationRepo.existsById(id);
    }

    public EducationResponseDto createEducation(EducationRequestDto educationRequestDto) {
        Education education = new Education();
        EducationResponseDto educationResponseDto = new EducationResponseDto();
        BeanUtils.copyProperties(educationRequestDto, education);
        educationRepo.save(education);
        BeanUtils.copyProperties(education, educationResponseDto);
        return educationResponseDto;
    }

    public void deleteEducation(Long id) {
        educationRepo.deleteById(id);
    }

}
