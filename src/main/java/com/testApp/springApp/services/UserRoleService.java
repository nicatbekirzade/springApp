package com.testApp.springApp.services;

import com.testApp.springApp.payload.request.UserRoleRequestDto;
import com.testApp.springApp.payload.response.UserRoleResponseDto;
import com.testApp.springApp.exceptions.UserAlreadyHasRoleEx;
import com.testApp.springApp.exceptions.UserRoleNotFoundEx;
import com.testApp.springApp.model.UserRole;
import com.testApp.springApp.repository.UserRoleRepo;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserRoleService {

    private final UserRoleRepo userRoleRepo;
    private final ModelMapper modelMapper;

    public UserRoleResponseDto createUserRole(UserRoleRequestDto userRoleRequestDto) {
        boolean existsByUserId = userRoleRepo.existsByUserId(userRoleRequestDto.getUserId());
        if (!existsByUserId) {
            UserRole userRole = modelMapper.map(userRoleRequestDto, UserRole.class);

//            Role role = modelMapper.map(userRoleRequestDto.getRole(), Role.class);
//            User user = modelMapper.map(userRoleRequestDto.getUser(), User.class);
//
//            userRole.setRole(role);
//            userRole.setUser(user);
            userRoleRepo.save(userRole);

            return modelMapper.map(userRole, UserRoleResponseDto.class);
        } else throw new UserAlreadyHasRoleEx();

    }

    public UserRoleResponseDto updateUserRole(Long id, UserRoleRequestDto userRoleRequestDto) {
        Optional<UserRole> byId = userRoleRepo.findById(id);
        if (byId.isPresent()) {
            UserRole userRole = byId.get();
            BeanUtils.copyProperties(userRoleRequestDto, userRole);
            userRoleRepo.save(userRole);
            return modelMapper.map(userRole, UserRoleResponseDto.class);
        } else throw new UserRoleNotFoundEx();
    }

    public UserRoleResponseDto findById(Long id) {
        Optional<UserRole> byId = userRoleRepo.findById(id);
        if (byId.isPresent()) {
            return modelMapper.map(byId.get(), UserRoleResponseDto.class);
        } else throw new UserRoleNotFoundEx();
    }
}
