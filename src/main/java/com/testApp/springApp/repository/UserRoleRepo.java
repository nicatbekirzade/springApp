package com.testApp.springApp.repository;

import com.testApp.springApp.model.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRoleRepo extends JpaRepository<UserRole, Long> {

    boolean existsByUserId(Long id);
}
