package com.testApp.springApp.repository;

import com.testApp.springApp.model.EducationTr;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface EducationTrRepo extends JpaRepository<EducationTr, Long> {

    List<EducationTr> findAllByEducationId(Long id);
    Optional<EducationTr> findByEducationIdAndLanguageId(Long eId, Long lId);

}
